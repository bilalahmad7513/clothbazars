﻿using ClothBazar.Database;
using ClothBazar.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothBazar.Services
{
    public class CategoriesService
    {
        public Category GetCategory(int ID)
        {
            using (var context = new CBContext())

            {
                return context.Categories.Find(ID);
            }
        }
        public List<Category> GetCaategories()
        {
            using (var context = new CBContext())

            {
                return context.Categories.ToList();
            }
        }
        public void SaveCategory(Category catgory)
        {
            using (var context = new CBContext())

            {
                context.Categories.Add(catgory);
                context.SaveChanges();
            }
        }
        public void UpdateCategory(Category catgory)
        {
            using (var context = new CBContext())

            {
                context.Entry(catgory).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
            }
        }
        public void DeleteCategory(int ID)
        {
            using (var context = new CBContext())

            {
                

                var category = context.Categories.Find(ID);

                context.Categories.Remove(category); 

                context.SaveChanges();
            }
        }
    }
}
